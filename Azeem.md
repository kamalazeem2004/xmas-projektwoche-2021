# XMas-IBZ-Projektwoche 2021: 
![](https://www.ipso.ch/themes/custom/beg/favicons/ibz/apple-touch-icon.png)
In dieser Woche werden wir uns mit den Sachen unten aufgelistet auseinandersetzten.
___
# Inhaltsverzeichnis
  [GitHub](#github)
  [GitLab](#gitlab)
  [Markdown](#markdown)
  [DevOps](#devops)
  [No more Microsoft](#no-more-microsoft)


## GitHub:
---
#### Geschichte:
GitHub wurde von Chris Wanstrath, PJ Hyett, Scott SShacon und Tom Preston Werner mittels Ruby on Rails  und Erlang entwickelt im Februar 2008 gestartet. 
#### Was ist GitHub?
GitHub ist eine Code-Hosting-Plattform. GitHub ist sehr geeignet für Gruppenarbeiten oder andere Projekte welche man teilen möchte. Man kann die neuste Version vom Projekt immer wieder auf Git heraufladen, sodass alle Kollegen, welche auch am Projekt arbeiten, auf dem neuesten sind. Das ist die effizienteste Arbeitsweise, wie man am besten von überall an IT-Projekten zusammen arbeiten kann.
#### Vorteile:
* Vereinfacht die Gruppenarbeiten
* Einfach Programme herunterladen
* Perfekter Ort um Arbeitsproben für Praktikumsstellen heraufzuladen
* Kostenlose Version bietet alles an was man braucht
* Man kann von überall auf gespeicherten Projekten zugreifen
* Versionen sind im Überblick, wenn etwas schieflauft kann man die alte Version wieder aufrufen
* Parallel an Projekten in verschiedenen branches arbeiten
* Basis-Mitgliederschaft ist kostenlos
* Grosse Community und leicht zu findende Hilfe

#### Nachteile:
* Es gibt Speicherplatzbeschränkungen 
* Eine Datei darf nicht grösser als 100 MB sein

*Profitieren Sie von diesem Tool. Eröffnen Sie Ihren GitHub Account*: [GitHub](https://docs.github.com/en/get-started/quickstart/hello-world)

## GitLab:
---
#### Geschichte:
GitLab wurde von den ukrainischen Entwicklern Dimitry Zaporozhets und Valery Sizov unter Verwendung der Programmiersprache Ruby und einiger Teile in Go geschrieben. Später wurde seine Architektur mit Go, Vue.js und verbessert Ruby on Rails, wie im Fall von GitHub.
#### Was ist GitLab?
GitLab ist eine weitere Alternative zu GitHub. GitLab wurde auch, um Open-Source-Projekte zu hosten entwickelt, aber es gibt einige Unterschiede zu GitHub. Diese Webseite bietet zu der Repository-Verwaltung und Versionskontrolle auch Hosting für Wikis und Bug-Tracking-System.
#### Vorteile:
* Vereinfacht die Gruppenarbeit
* Kostenloser Plan ohne Einschränkung, obwohl es Zahlungspläne gibt
* Es hat ein eigenes Wiki
* Es ist eine Open-Source-Lizenz
* Es ist sehr übersichtlich

#### Nachteile:
* Es gibt Speicherplatzbeschränkungen 
* Die Benutzeroberfläche ist im Vergleich zu GitHub etwas langsamer
* Es gibt häufig Probleme mit Repositorys
* eEtwas schwierig zu verstehen
## Markdown:
----

#### Geschichte:
Markdown wurde von John Gruber und Aron Swart entworfen und im Dezember 2004mit der Version 1.0.1 spezifiziert.
#### Was ist Markdown?
Markdown ist eine Auszeichnungssprache, deren Hauptabsicht Lesbarkeit ist. Es ermöglicht uns, Texte im Web zu formatieren, ohne das Dokument mit eckigen Klammern oder sonstigen Befehlen zu überfüllen, wie man sie für ein HTML-Dokument normalerweise braucht. 
#### Wieso Markdown und nicht HTML?
Markdown ist viel übersichtlicher und einfacher zu lernen. Die Syntax ist sehr einfach einzuprägen.
#### Vorteile:
* Es ist sehr einfach zu lernen
* Es ist ein übersichtlicher im Gegensatz zum HTML Code
* Die meisten Editoren können Markdown
* Es gibt viele Tutorials zu Markdown
* Praktisch

#### Nachteile:
* Keine bekannt

## DevOps
---
#### Was ist DevOps?
DevOps ist eine anhäufung von verschiedener Technischer Methoden und eine Kultur für die Zusammenarbeit von Applikationsentwicklern und Systemtechickern. Mit dieser Methode kann man besser, schneller und sicherer arbeiten und Anwendungen sind organisierter. In anderen Worten ist DevOps ein Zusammenarbeits-Modell.






